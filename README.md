# aero-fpga
FPGA code to support Intel Aero configurations

- _aero_rtf_kit_: original intel program
- _aero_sample_: original sample program
- _aero_rtf_kit_flair_: program to use with flair. Motors are connected to CPU

# build instructions:

- download and install [intel quartus lite](https://www.intel.com/content/www/us/en/collections/products/fpga/software/downloads.html)
- open _qpf_ project file in quartus
- launch _start compilation_ to build a _pof_ file
- convert the _pof_ file to a _jam_ file:
    - export PATH=$PATH:~/intelFPGA_lite/23.1std/quartus/linux64
    - export LD_LIBRARY_PATH=~/intelFPGA_lite/23.1std/quartus/linux64/
    - run make in the project directory


